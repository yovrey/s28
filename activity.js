// [insertOne method]

db.activity.insert({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all basic necessities",
	rooms_available: 10,
	isAvailable: false
})




//[insertMany method]

db.activity.insertMany([
	{
	name: "double",
	accomodates: 3,
	price: 2000,
	description: "A room fit for a small family going on a vacation",
	rooms_available: 5,
	isAvailable: false
	},
	{
	name: "queen",
	accomodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple getaway",
	rooms_available: 15,
	isAvailable: false
	}
])


// [Find method]

db.activity.find({
	name: "double"
})



// [updateOne method]

db.activity.updateOne(
	{name: "queen"},
	{
		$set: {rooms_available: 0}
	}
)

// [delateMany method]

db.activity.deleteMany(
	{
		rooms_available: 0
	}
)